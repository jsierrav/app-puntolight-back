'use strict'
var mongoose = require('mongoose');
var app = require('./app');
var port = process.env.port || 3678;

mongoose.connect('mongodb://localhost:27017/appPedidos', (error, result) => {

    if (error) {
        throw error;
    } else {
        console.log("Conexion correcta");
        app.listen(port, () => {
            console.log('api resume manager listening on port' + port)
        })
    }
});