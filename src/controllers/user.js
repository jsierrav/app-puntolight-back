'use strict'

var User = require("../models/user");


function getById(request, result) {
    var id = request.params.id;

    User.findById(id, (error, user) => {
        if (error) {
            request.status(500).send({
                message: "Ha ocurrido un error"
            })
        } else {

            if (!category) {
                request.status(404).send({
                    message: "No existe este usuario"
                })
            }
            result.status(200).send({
                result: user
            });

        }
    })
}


function get(request, result) {
    User.find({}).sort('-name').exec((error, user) => {
        if (error) {
            result.status(500).send({
                message: 'Ha ocurrido un error'
            });
        } else {
            if (!user) {
                result.status(404).send({
                    message: 'No hay usuarios'
                })
            }

            result.status(200).send(user);

        }
    })

}


function login(request, result) {

    var params = request.body;

    console.log(params);
    User.findOne({code:params.code,password:params.password}, (error, user) => {
        if (error) {
            result.status(500).send({
                message: 'Ha ocurrido un error'
            });
        } else {

            if(!user){
                result.status(200).send({message:"0"});
                
            }else{
                result.status(200).send({message:"1", user:user})
            }
        }
    })




}

function save(request, result) {

    var params = request.body;
    var user = new User();
    user.fullName = params.fullName;
    user.lastName = params.lastName;
    user.mail = params.mail;
    user.password =params.password;
    user.code = params.code;
    user.type = params.type;
    user.save((error, userStore) => {
        if (error) {
            result.status(400).send({
                message: 'Error al guardar el usuario'
            })
        }

        result.status(200).send({
            user:userStore
        })
    })

}

function update(request, res) {
    var id = request.params.id;
    var update = request.body;

    User.findByIdAndUpdate(id, update, (error, userUpdate) => {
        if (error) {
            res.status(500).send({
                message: 'Error al editar el usuario'
            });
        } else {
            res.status(200).send({
                user: userUpdate
            })
        }
    })
}

function deleteUser(request, result) {
    var id = request.params.id;
    User.findByIdAndRemove(id, (error, user) => {
        if (error) {
            result.status(500).send({
                message: 'Ha ocurrido un error'
            })
        }
        if (!user) {
            result.status(404).send({
                message: 'no existe el usuario'
            })
        } else {
            user.remove(error => {
                if (error) {
                    result.status(500).send({
                        message: 'Error al intentar borrar el usuario'
                    })
                } else {
                    result.status(200).send({
                        message: 'Se ha eliminado el usuario'
                    })
                }
            })
        }
    });
}


module.exports = {
    save,
    getById,
    get,
    deleteUser,
    update,
    login
}