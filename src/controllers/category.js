
'use strict'

var Category = require("../models/category");


function getById(request, result) {
    var id = request.params.id;

    Category.findById(id, (error, category) => {
        if (error) {
            request.status(500).send({
                message: "Ha ocurrido un error"
            })
        } else {

            if (!category) {
                request.status(404).send({
                    message: "No existe esta categoria"
                })
            }
            result.status(200).send({
                result: category
            });

        }
    })
}


function get(request, result) {
    Category.find({}).sort('-name').exec((error, category) => {
        if (error) {
            result.status(500).send({
                message: 'Ha ocurrido un error'
            });
        } else {
            if (!category) {
                result.status(404).send({
                    message: 'No hay categorias'
                })
            }

            result.status(200).send({result:category});

        }
    })

}


function getByName(request, result) {
    Category.findOne({name:new RegExp('^'+request.params.name+'$', "i")}, (error, category) => {
        if (error) {
            result.status(500).send({
                message: 'Ha ocurrido un error'
            });
        } else {
        

            result.status(200).send(category);

        }
    })




}

function save(request, result) {

    var params = request.body;
    var category = new Category();
    category.name = params.name;
    category.description = params.description;
    category.save((error, categoryStore) => {
        if (error) {
            result.status(400).send({
                message: 'Error al guardar la categoria'
            })
        }

        result.status(200).send({
            category:categoryStore
        })
    })

}

function update(request, res) {
    var id = request.params.id;
    var update = request.body;

    Category.findByIdAndUpdate(id, update, (error, categoryUpdate) => {
        if (error) {
            res.status(500).send({
                message: 'Error al editar la categoria'
            });
        } else {
            res.status(200).send({
                category: categoryUpdate
            })
        }
    })
}

function deleteCategory(request, result) {
    var id = request.params.id;
    Category.findByIdAndRemove(id, (error, category) => {
        if (error) {
            result.status(500).send({
                message: 'Ha ocurrido un error'
            })
        }
        if (!category) {
            result.status(404).send({
                message: 'no existe la category'
            })
        } else {
            category.remove(error => {
                if (error) {
                    result.status(500).send({
                        message: 'Error al intentar borrar la categoria'
                    })
                } else {
                    result.status(200).send({
                        message: 'Se ha eliminado la categoria'
                    })
                }
            })
        }
    });
}


module.exports = {
    save,
    getById,
    get,
    deleteCategory,
    update,
    getByName
}