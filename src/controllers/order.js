'use strict'

var Order = require('../models/order');
var Product = require('../models/product');


function save(request, res) {


    var order = new Order();
    order.orderId = request.body.orderId;
    order.status = request.body.status;
    order.total = request.body.total;
    order.product = request.body.product;
    order.user = request.body.user;

    console.log(order);

    order.save((error, orderStore) => {
        if (error) {
            res.status(400).send({
                message: "Error al guardar la información"
            })
        } else {
            res.status(200).send({
                order: orderStore
            })
        }
    })
}

function update(request, res) {
    var id = request.params.id;
    var update = request.body;

    Order.findByIdAndUpdate(id, update, (error, orderUpdate) => {
        if (error) {
            res.status(500).send({
                message: 'Error al editar el pedido'
            });
        } else {
            res.status(200).send({
                result: orderUpdate
            })
        }
    })
}

function getById(request, result) {

    var id = request.params.id;
    Order.findById(id).populate("user").exec((error, order) => {
        if (error) {
            result.status(500).send({
                message: "Ha ocurrido error"
            })
        } else {
            result.status(200).send({
                result: order
            });

        }
    })
}

function get(request, result) {
    Order.find({}).populate("user").exec((error, orders) => {
        if (error) {
            result.status(500).send({
                message: "Ha ocurrido error"
            })
        } else {
            result.status(200).send({
                result: orders
            });

        }
    })
}

function getByStatus(request, result) {

    var status = request.params.status;
    Order.find({
        status: status
    }).populate("user").exec((error, orders) => {
        if (error) {
            result.status(500).send({
                message: "Ha ocurrido error"
            })
        } else {
            result.status(200).send({
                result: orders
            });

        }
    })
}

function getCarrito(request, result) {
    
        var status = request.params.status;
        var userId = request.params.userId;
        
        Order.find({
            status: status,user:userId
        }).populate("user").exec((error, orders) => {
            if (error) {
                result.status(500).send({
                    message: "Ha ocurrido error"
                })
            } else {
                result.status(200).send({
                    result: orders
                });
    
            }
        })
    }

function updateStatus(request, res) {
    var id = request.params.id;
    var status = request.params.status;
    Order.findByIdAndUpdate(id, {
        status: status
    }, (error, orderUpdate) => {
        if (error) {
            res.status(500).send({
                message: 'Error'
            });
        } else {
            if (!orderUpdate) {
                res.status(400).send({
                    message: "No existe esta orden"
                });
            } else {
                res.status(200).send({
                    result: orderUpdate
                })
            }
        }

    })
}



function deleteOrder(request, result) {
    var id = request.params.id;
    Order.findByIdAndRemove(id, (error, order) => {
        if (error) {
            result.status(500).send({
                message: 'Ha ocurrido un error'
            })
        }
        if (!order) {
            result.status(404).send({
                message: 'no existe el pedido'
            })
        } else {
            order.remove(error => {
                if (error) {
                    result.status(500).send({
                        message: 'Error al intentar borrar el pedido'
                    })
                } else {
                    result.status(200).send({
                        message: 'Se ha eliminado la orden'
                    })
                }
            })
        }
    });
}


module.exports = {
    save,
    getById,
    get,
    update,
    deleteOrder,
    getByStatus,
    updateStatus,
    getCarrito

}