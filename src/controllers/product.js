'use strict'

var Product = require('../models/product');
var path = require('path');

function save(request, result) {
    var params = request.body;
    var categoryId = request.params.categoryId;

    var product = new Product();

    product.name = params.name;
    product.price = params.price;
    product.time = params.time;
    product.categoryId = categoryId;
    product.image = null;

    product.save((error, productStore) => {
        if (error) {
            result.status(400).send({
                message: 'Error al guardar el producto'
            })
        } else {

            result.status(200).send({
                result: productStore
            })
        }
    })

}


function getByCategory(request, result) {
    var categoryId = request.params.categoryId;

    var find;
    if (!categoryId) {
        find = Product.find({}).sort('-name');
    } else {
        find = Product.find({
            categoryId: categoryId
        }).sort('-name');
    }

    find.exec((error, product) => {
        if (error) {
            result.status(500).send({
                message: 'Ha ocurrido a error'
            });


        } else {
            if (!product) {
                result.status(404).send({
                    message: 'No hay productos para esta categorìa'
                });
            } else {
                result.status(200).send({
                    result: product
                });
            }
        }
    })

}

function get(request, result) {
    Product.find({}).sort('-name').exec((error, product) => {
        if (error) {
            result.status(500).send({
                message: 'Ha ocurrido un error'
            });
        } else {
            if (!product) {
                result.status(404).send({
                    message: 'No hay productos'
                })
            }

            result.status(200).send({
                result: product
            });

        }
    })

}

function update(request, res) {
    var id = request.params.id;
    var update = request.body;

    Product.findByIdAndUpdate(id, update, (error, productUpdate) => {
        if (error) {
            res.status(500).send({
                message: 'Error al editar la categoria'
            });
        } else {
            res.status(200).send({
                product: productUpdate
            })
        }
    })
}

function deleteProduct(request, result) {
    var id = request.params.id;
    Product.findByIdAndRemove(id, (error, product) => {
        if (error) {
            result.status(500).send({
                message: 'Ha ocurrido un error'
            })
        }
        if (!product) {
            result.status(404).send({
                message: 'no existe el producto'
            })
        } else {
            product.remove(error => {
                if (error) {
                    result.status(500).send({
                        message: 'Error al intentar borrar el producto'
                    })
                } else {
                    result.status(200).send({
                        message: 'Se ha eliminado el producto'
                    })
                }
            })
        }
    });
}

function uploadImage(req, res) {
    var imageId = req.params.id;
    var file_name = 'No upload..';

    if (req.files) {
        var file_path = req.files.image.path;

        var file_split = file_path.split('/');
        file_name = file_split[1];
            
        console.log(file_split);
        console.log(file_name);


        Product.findByIdAndUpdate(imageId, {
            image: file_name
        }, (error, productUpdate) => {
            if (error) {
                res.status(500).send({
                    message: 'Error'
                });
            } else {
                if (!productUpdate) {
                    res.status(400).send({
                        message: "No existe este producto"
                    });
                } else {
                    res.status(200).send({
                        result: productUpdate
                    })
                }
            }

        })
    } else {
        res.status(500).send({
            message: "No ha subido una imagen"
        })
    }

}



function getById(request, result) {
    var id = request.params.id;

    Product.findById(id, (error, product) => {
        if (error) {
            request.status(500).send({
                message: "Ha ocurrido un error"
            })
        } else {

            if (!product) {
                result.status(404).send({
                    message: "No existe este producto"
                })
            }
            result.status(200).send({
                result: product
            });

        }
    })
}



var fs = require('fs');

function getImageFile(req, res) {


    var imageFile = req.params.file;
    fs.exists('./images/' + imageFile, function (exists) {
        if (exists) {
            res.sendFile(path.resolve('./images/' + imageFile));

        } else {
            res.status(200).send({
                message: 'No exise la imagen'
            });
        }



    })



}



module.exports = {
    save,
    getByCategory,
    get,
    getById,
    save,
    deleteProduct,
    update,
    uploadImage,
    getImageFile

}