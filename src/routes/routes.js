'use strict'

var express = require('express');

var categoryController = require('../controllers/category');
var productController = require('../controllers/product');
var userController = require('../controllers/user');
var orderController = require('../controllers/order');

var api = express.Router();

var multipart = require('connect-multiparty');
var multipartMiddleware = multipart({uploadDir:'./images'})

api.post('/category', categoryController.save);
api.put('/category/:id',categoryController.update);
api.delete('/category/:id',categoryController.deleteCategory);
api.get('/category/:id',categoryController.getById);
api.get('/category/search/:name',categoryController.getByName);

api.get('/category',categoryController.get);



api.post('/product/:categoryId', productController.save);
api.get('/product/:categoryId', productController.getByCategory);
api.get('/product-detail/:id', productController.getById);

api.get('/product', productController.get);
api.put('/product/:id', productController.update);
api.delete('/product/:id', productController.deleteProduct);
api.post('/upload-product/:id',multipartMiddleware,productController.uploadImage);
api.get('/product-image/:file', productController.getImageFile);


api.post('/user', userController.save);
api.post('/login', userController.login);

api.put('/user/:id', userController.update);
api.delete('/user/:id', userController.deleteUser);
api.get('/user',userController.get);
api.get('/user/:id',userController.getById);


api.post('/order',orderController.save);
api.get('/order/:id',orderController.getById);
api.get('/order',orderController.get);
api.get('/order/status/:status/user/:userId',orderController.getCarrito);

api.get('/order/status/:status',orderController.getByStatus)
api.put('/order/:id',orderController.update);
api.put('/order/:id/status/:status',orderController.updateStatus);

api.delete('/order/:id',orderController.deleteOrder);








module.exports = api;