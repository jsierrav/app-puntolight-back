
var mongoose = require('mongoose');
var schema = mongoose.Schema;

var userSchema = mongoose.Schema({
    fullName:String,
    lastName:String,
    mail:String,
    code:String,
    password: String,
    type:String
})

module.exports = mongoose.model('user', userSchema);