'use strict'

var mongoose = require('mongoose');
var schema = mongoose.Schema;

var categorySchema = mongoose.Schema({
    name:String,
    description:String
})

module.exports = mongoose.model('category',categorySchema);
