'use strict'

var mongoose = require('mongoose');
var schema = mongoose.Schema;

var productSchema = mongoose.Schema({
    name: String,
    price: String,
    time: String,
    image:String,
    categoryId:String
})

module.exports = mongoose.model('products', productSchema);