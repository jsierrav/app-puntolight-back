'use strict'
var mongoose = require('mongoose');
var schema = mongoose.Schema;

var orderSchema = mongoose.Schema({
    orderId: String,
    status: String,
    total: Number,
    product: [String],
    user: {
        type: schema.Types.ObjectId,
        ref: 'user'
    }
})

module.exports = mongoose.model('order', orderSchema);