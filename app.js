'use strict'
var express = require('express');
var bodyParser = require('body-parser');
var swaggerJSDoc = require('swagger-jsdoc');
var api = require('./src/routes/routes');


var app = express();

app.use(bodyParser.json({limit:'50mb'})); 
app.use(bodyParser.urlencoded({extended:false, limit:'50mb'}));


  
  // initialize swagger-jsdoc
//var api = require('./src/routes/vacantie');




app.use((request, result, next) => {
    result.header('Access-Control-Allow-Origin', '*');
    result.header('Access-Control-Allow-Headers', 'X-API-KEY, Origin, X-Requested-With, Content-Type, Accepte, Access-Control-Request-Method');
    result.header('Access-Control-Allow-Methods', 'GET,PUT, POST, OPTIONS, DELETE');
    result.header('Access', 'GET,PUT, POST, OPTIONS, DELETE');

    next();
})



app.use('/api', api);

module.exports = app;